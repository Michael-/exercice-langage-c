#include <stdio.h>
#include <stdlib.h>

int main()
{

/* Exercice boucles n1 : -*--------------------
*Ecrire un programme qui calcul la somme des entiers de 1 � N.
*entr�e : nombre
*sortie : somme
*/
/*
int som =0;
int n;
printf("S=");
for(n=1;n<=7;n++)
    {
    som+=n;
    printf("%d+",n);

    }
    som+=n;
    printf("%d",n);
    printf("=%d",som);
*/

/* Exercice boucles n2 : -*--------------------
*Ecrire un programme qui lit un nombre entier au clavier et qui affiche leur somme, produit,moyenne
*entr�e : nombre au clavier
*sortie : somme,produit,moyenne
*/
/*
int d;
int n1;
int p=1;
int c=1;
int s=0;
double m=0;
int mu=1;
int div;

/*
printf("Entrez le nombre de donnees souhaite :\n");
scanf("%d",&d);
printf("Nombre de donnees : %d\n",d);
*/
// Avec while --------------------------------------------------
/*
while (c<=d)
{
    scanf("%d",&n1);
    c++;
    s+=n1;
    p*=n1;
    m=(double)s/d;

}

printf("La somme des %d nombres est %d\n",d,s);
printf("Le produit des %d nombres est %d\n",d,p);
printf("La moyenne des %d nombres est %.2lf\n",d,m);
*/

// Avec for -------------------------------------------------------

/*
for(c=0;c<d;c++)
{
    scanf("%d",&n1);
    s+=n1;
    p*=n1;
    m=(double)s/d;

}

printf("La somme des %d nombres est %d\n",d,s);
printf("Le produit des %d nombres est %d\n",d,p);
printf("La moyenne des %d nombres est %.2lf\n",d,m);
*/

/*  Exercice boucles n3 : -*--------------------
*Calculer par des additions successives le produit de deux nombres entiers entr�s au clavier
*entr�e :  deux nombres entiers
*sortie :  Exemple : 5*8 = 5+5+5+5+5+5+5+5 soit 40.
*/
/*
printf("Entrez deux nombres entier :\n");
scanf("%d",&d);
scanf("%d",&n1);
printf("%d*%d = ",d,n1);

    while(c<n1)
    {
        s+=d;
        c++;
        printf("%d + ",d);

    }

    s+=n1;
    printf("%d",d);
    printf(" = %d",s);


/*  Exercice boucles n4 : -*--------------------
*Calculer par des soustractions successives le quotient entier et le reste de la division entiere de deux nombres entiers entr�s au clavier
*entr�e :  deux nombres entiers
*sortie :  le reste et qotient
*/


/*
int d;
int n;
int div=0;

printf("Numerateur : ");
scanf("%d",&n);
printf("Denominateur : ");
scanf("%d",&d);

int reste=n;

 while(reste>=d)
    {
        reste-=d;
        div++;
    }

printf(" %d divise par %d est %d reste %d\n", n, d, div, reste);

*/





/*  Exercice boucles n5 : -*--------------------
* calculatrice -- Ecrire un programme qui demande � l�utilisateur un type d�op�ration qu�il veut effectuer (on peut commencer par +, -, * et /).
*entr�e :  nombres
*sortie :  resultat de l'operation
*/

int n;
int a;
int n1;
char non = 'o';

while(non=='o')
{
printf("Quel type d operation voulez vous effectuer ? Choix 1-[+] 2-[-] 3-[*] 4-[/]\n");
scanf("%d",&a);
printf("Saisissez deux nombres\n");
scanf("%d",&n);
scanf("%d",&n1);


    switch(a)
    {
        case 1 : printf("%d + %d = %d\n",n,n1,n+n1);
        break;
        case 2 : printf("%d - %d = %d\n",n,n1,n-n1);
        break;
        case 3 : printf("%d * %d = %d\n",n,n1,n*n1);
        break;
        case 4 : printf("%d / %d = %d\n",n,n1,n/n1);
        break;
    }

    if(non=='n')
    {
        break;
    }
    printf("Tapez n pour arreter ou o pour continuer\n");
    scanf("%s",&non);
}

    return 0;
}

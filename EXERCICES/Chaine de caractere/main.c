#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#define n 100
int longueurchaine(const char chaine[])
{
    int i=0;
    while (chaine[i]!='\0')
    {
        i++;
    }
    return i;
}

char majchartomin(char c)
{
    if((c>64)&&(c<91))
    {
       return c+=32;
    }
}

char mintomaj(char c)
{
    if((c>96)&&(c<123))
    {
    return c-=32;
    }
}

char invcharminmaj(char c)
{
    if((c<96)&&(c>64))
    {
        c+=32;
        return printf("%c",c);
    }
    else
    {
        c-=32;
        return printf("%c",c);
    }
}

void majstringtomin(char chaine[],char newchaine[])
{
    int i=0;
    while(chaine[i]!='\0')
    {
        if((chaine[i]>64)&&(chaine[i]<91))
        {
            newchaine[i]=chaine[i]+32;
        }
        i++;
    }
}

void minstringtomaj(char chaine[],char newchaine[])
{
    int i=0;
    while(chaine[i]!='\0')
    {
        if((chaine[i]>96)&&(chaine[i]<122))
        {
            newchaine[i]=chaine[i]-32;
        }
        i++;
    }
}

void invstringminmaj(char str[],char newstr[])
{
    int i = 0;
    while(str[i]!='\0')
    {
        if((str[i]>96)&&(str[i]<122))
        {
            newstr[i]=str[i]-32;
            i++;
        }

        else if((str[i]>64)&&(str[i]<91))
        {
            newstr[i]=str[i]+32;
            i++;
        }

    }
}

int comparestr(char str[],char newstr[])
{
    int i =0;
    int c=0;
    if(longueurchaine(str)==longueurchaine(newstr))
    {
        while(str[i]!='\0')
        {
            if(majchartomin(str[i])==majchartomin(newstr[i]))
            {
                c++;
            }
            i++;
        }
    }
    return longueurchaine(str)==c;
}

void supprimerelement(int nbelt,int pos,char str[])
{
    for(int i= pos-1;i<nbelt;i++)
    {
        str[i]=str[i+1];
    }
}

int estvoyelle(char c)
{
    if((c==65)||(c==69)||(c==73)||(c==79)||(c==85)||(c==89)||(c==97)||(c==101)||(c==105)||(c==111)||(c==117)||(c==121))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}


void supprimevoyelles(char str[],char newstr[])
{
    int i=0;
    while(str[i]!='\0')
    {
        if(((str[i]>64)&&(str[i]<91))&&((str[i]>96)&&(str[i]<122))&&(str[i]==65)||(str[i]==69)||(str[i]==73)||(str[i]==79)||(str[i]==85)||(str[i]==89)||(str[i]==97)||(str[i]==101)||(str[i]==105)||(str[i]==111)||(str[i]==117)||(str[i]==121))
        {
            supprimerelement(longueurchaine(str),i+1,str);
            i--;
        }

    newstr[i]=str[i];
    i++;
    }
}

//Page 2 ---------------------------------------------------------


int estchiffre(char c)
{
    if((c<58)&&(c>47))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

int estlettre(char c)
{
    if(((c>=65)&&(c<=90))||((c<=122)&&(c>=97)))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

int estalphanumerique(char c)
{
    if(estlettre(c)||estchiffre(c))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

int estnombredecimal(char str[])
{
    int i=0;
    int c=0;
    while(str[i] != '\o')
    {
        if (estchiffre(str[i]))
        {
            c++;
        }
        i++;
    }
    return longueurchaine(str) == c;
}

int compte(char c, char s[])
{
    int i=0;
    int compt=0;
    for(i=0;i<longueurchaine(s);i++)
    {
        if(s[i]==c)
        {
            compt++;
        }
    }
    return compt;
}

char lettrelaplusfrequente(char str[])
{
    char c=0;
    int nblt=0;
    for(int i=0;i<longueurchaine(str);i++)
    {
        if(estlettre(str))
        {
            int nbl=compte(str[i],str);
            if(nbl>nblt)
            {
                nblt=nbl;
                c=str[i];
            }

        }
    }
    return c;
}

int main()
{

/*Chaines de caract�res 1 --------------------------------------------------------
*Exercice 1 ------------- http://www.callac-soft-college.fr/Exercices-Algorithmiques-C/
*Ecrire une fonction qui permet de calculer la longueur d'une chaine de caract�res
*Entr� : fonction
*sortie : longueur de la chaine
*/
/*
char chaine[7];
chaine[0]='B';
chaine[1]='o';
chaine[2]='n';
chaine[3]='j';
chaine[4]='o';
chaine[5]='u';
chaine[6]='r';
chaine[7]='\0';

printf("%d\n",longueurchaine(chaine));
*/
/*Chaines de caract�res 1 --------------------------------------------------------
*Exercice 2 -------------
*Ecrire une fonction qui transforme le caract�re minuscule en majuscule
*Entr� : majuscule
*sortie : minuscule
*/

//char chaine[n]="J";
/*majchartomin('Z');
printf("\n");

mintomaj('z');
printf("\n");

invcharminmaj('a');
invcharminmaj('B');
invcharminmaj('c');
invcharminmaj('D');
printf("\n");*/

/*char chaine[n]="COUCOU";
char newchaine[n]="";

majstringtomin(chaine,newchaine);
printf("%s",newchaine);

char chaine[n]="coucou";
minstringtomaj(chaine,newchaine);
printf("%s",newchaine);
*/

/*char str[n]="CoUcOu";
char newstr[n]="";

invstringminmaj(str,newstr);
printf("%s",newstr);*/
/*
char str[n]="Phrase tEst";
char newstr[n]="Phrase test";
printf("%d",comparestr(str,newstr));
*/
/*
char str[n]="BOoOoOoOOOoooonnNNNjJJJJOOooouuuUUrrr";
char newstr[n]="";
supprimevoyelles(str,newstr);
printf("%s",newstr);
*/
/*
char c='m';
printf("%d",estvoyelle(c));
*/

/*------------------------------Chaines de caract�res II -----------------------
*Exercice 1--------------
*/


//char str[n] = "99a99";
/*
printf("%d\n",estchiffre(c));

printf("%d\n",estalphanumerique(c));

printf("%d\n",estlettre(c));

printf("%d",estnombredecimal(str));
*/

/*Exercice 2 ---------------------------------------------------
*2-	Ecrire dans la fonction main (), un programme qui permet de compter le nombre de lettres de l�alphabet,
le nombre de chiffres, et le nombre de caract�res non alphanum�riques dans une chaine de caract�res donn�e.
*/

/*
char str[n]="tout va tres bien depuis 3 jours";

int i=0;
int c=0;
int c1=0;
int c2=0;
while(str[i]!='\0')
{
    if(estchiffre(str[i]))
    {
        c++;
    }
    else if(estlettre(str[i]))
    {
        c1++;
    }
    else
    {
        c2++;
    }
    i++;
}
printf("Il y a %d chiffres, %d lettres et %d caracteres non alphanumeriques",c,c1,c2);

*/

/*Exercice 3 ---------------------------------------------------
*-	Ecrire la fonction int compte (char c, char s []) qui compte le nombre d�occurrences du caract�re c dans la cha�ne de caract�res s.
Exemple : dans bonjour il y a 2 caract�res �o�.
*/

/*
char s[n]="Bonjour";
printf("Dans bonjour il y a %d caractere 'o'",compte('o',s));
*/

/*Exercice 4 ---------------------------------------------------
*-�crire un programme char lettreLaPlusFrequente (char str [])  qui renvoie la lettre
(minuscule ou majuscule) la plus fr�quente d�une cha�ne de caract�res.
*/

char s[n]="Bonjour";
printf("%s",lettrelaplusfrequente(s));

    return 0;
}

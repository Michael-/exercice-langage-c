#include <stdio.h>
#include <stdlib.h>


int affichetab(int nbelt,int tab[])
{
    printf("Tableau resultat :\n");
    for (int i=0; i<nbelt; i++)
        {
        printf("%d ", tab[i]);
        }
}

void ajouterendernier(int nbelt,int tab[],int nbtoadd)
{
    tab[nbelt]=nbtoadd;
}
/*
void ajouterenpremier(int nbelt, int tab[], int nbtoadd)
{
    int v=tab[0];
    for(int i=0;i<nbelt;i++)
    {
        int v2=tab[i+1];
        tab[i+1]=v;
        v=v2;
        tab[0]=nbtoadd;
    }
}*/

void ajouterenpremier(int nbelt, int tab[], int nbtoadd)
{
    for(int i=nbelt;i>0;i--)
    {
        tab[i]=tab[i-1];
    }
    tab[0]=nbtoadd;
}

void insererelement(int nbelt, int tab[],int pos, int nbtoadd)
{

    int v=tab[pos];
    for(int i=pos;i<nbelt;i++)
    {
        int v2=tab[i+1];
        tab[i+1]=v;
        v=v2;

    }
    tab[pos]=nbtoadd;
}

void supprimerpremierelement(int nbelt,int tab[])
{
    int v2=tab[nbelt];
    for(int i=nbelt;i>0;i--)
    {
        int v=tab[i-1];
        tab[i-1]=v2;
        v2=v;
    }
}
void supprimerelement(int nbelt,int pos,int tab[])
{
    for(int i=0;i<nbelt;i++)
    {
        tab[i]=tab[i+1];
    }
}
int main()
{

  /*Exercice 1 ----------------------------------------
  *Fonction ajouterendernier
  *Entr� : Elements du tableau
  *sortie : ajoute l�entier nbToAdd a la fin du tableau tab
  */

int nbelt=5;
int tab[100]={2,4,6,8,10};

/*
ajouterendernier(nbelt,tab,12);
nbelt++;
affichetab(nbelt,tab);
printf("\n");printf("\n");
*/

 /*Exercice 2 ----------------------------------------
  *Fonction ajouterenpremier
  *Entr� : Elements du tableau
  *sortie : ajoute l�entier nbToAdd au debut du tableau tab
  */

/*
ajouterenpremier(nbelt,tab,12);
nbelt++;
affichetab(nbelt,tab);
printf("\n");printf("\n");
*/

 /*Exercice 3 ----------------------------------------
  *Fonction inserer element
  *Entr� : Elements du tableau
  *sortie : ajoute un element dans un tableau tab � la position pos
  */

/*
insererelement(nbelt,tab,4,12);
nbelt++;
affichetab(nbelt,tab);
printf("\n");printf("\n");
*/

 /*Exercice 4 ----------------------------------------
  *Fonction supprimer le premier element
  *Entr� : Elements du tableau
  *sortie : supprimer un element dans un tableau tab � la premi�re position
  */

/*
supprimerpremierelement(nbelt,tab);
nbelt--;
affichetab(nbelt,tab);
printf("\n");printf("\n");
*/

 /*Exercice 5 ----------------------------------------
  *Fonction supprimer un element
  *Entr� : Elemensupprimerelement(nbelt,3,tab);ts du tableau
  *sortie : supprimer un element dans un tableau tab
  */


supprimerelement(nbelt,3,tab);
nbelt--;
affichetab(nbelt,tab);
printf("\n");printf("\n");


    return 0;
}

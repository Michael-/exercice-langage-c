#include <stdio.h>
#include <stdlib.h>

void affichertab (int taille, int tableau[])
{
   for(int i=0;i<taille;i++)
   {
       printf("%d\n",tableau[i]);
   }
}

void remplirtab(int taille, int tableau[])
{
    for(int i=0;i<taille;i++)
    {
        printf("Donner un entier du tableau\n");
        scanf("%d",&tableau[i]);
        printf("La case : %d correspond a : %d\n ",i,tableau[i]);
    }
}

int sommertab(int taille, int tableau[])
{
    int somme;
        for(int i=0;i<taille;i++)
        {
            somme+=tableau[i];
        }
        printf("La somme des elements contenus dans le tableau est : %d\n",somme);
}

int sommercarretab(int taille, int tableau[])
{
    int sommecarre;
    for(int i=0;i<taille;i++)
    {
        tableau[i]*=tableau[i];
        sommecarre += tableau[i];
        printf("La somme des carr�s des elements contenus dans le tableau esst : %d\n",sommecarre);
    }
}

double moyennetab(int taille,int tableau[])
{
    int somme=0;

        for(int i=0;i<taille;i++)
        {
            somme+=tableau[i];
        }
         double moyenne = (double)(somme/taille);
        printf("La moyenne des elements contenus dans le tableau est : %.2lf\n",(double)moyenne);
}

int max(int taille, int tableau[])
{
    int v=0;
    for(int i=0;i<taille;i++)
    {
        if(tableau[i]>v)
        {
            v=tableau[i];
        }
    }
    return v;
}

int min(int taille, int tableau[])
{
    int v= max(taille,tableau);
    for(int i=0;i<taille;i++)
    {
        if(v>tableau[i])
        {
            v=tableau[i];
        }
    }
    return v;
}

void minmax(int taille, int tableau[],int tableauminmax[])
{
  tableauminmax[0]=min(taille,tableau);

  tableauminmax[1]=max(taille,tableau);

}

int testoccurrence(int taille,int tableau[],int nb)
{
    for(int i=0;i<taille;i++)
    {
        //printf("Entrez la valeur du tableau a verifier\n");
        //scanf("%d",&nb);
        if(tableau[i]==nb)
        {
            return 1;
        }

        else
        {
            return 0;
        }

    }
}

int calculnboccurences(int taille, int tableau[],int nb)
{
    int nb2=0;
    for(int i=0;i<taille;i++)
    {
        if(tableau[i]==nb)
        {
           nb2++;
        }
    }
        return nb2;
}

int pospremiereoccurrence(int taille, int tableau[],int nb)
{
    for(int i=0;i<taille;i++)
    {
        if(tableau[i]==nb)

        return i;
    }
}

void tableauoccurrences(int taille,int tableau[],int tableauindexes[],int nb)
{
    int v=0;
    for(int i=0;i<taille;i++)
    {
        if(tableau[i]==nb)
        {
            tableauindexes[v]=i+1;
            v++;
        }
    }

}
void inversertab(int taille,int tableau[],int tabinv[])
{
    int v=0;
    for(int i=taille-1;i>=0;i--)
    {
        tabinv[v]=tableau[i];
        v++;
    }
}


int inversertab2(int taille, int tableau[])
{
    int v;
    int nt;
    for(int i=0,v=taille-1;i<v;i++,v--)
    {
            nt=tableau[i];
            tableau[i]=tableau[v];
            tableau[v]=nt;
    }

}

int main()
{

 /* Exercice tableau ------------------------------------
 *
 *
 * Excercice 1 ---------------------
 *Ecrire une fonction void afficherTab
 *Entr�e : Argument du tableau
 *Sortie : afficher tableau d'entiers
 */

// int tableau[5]={2,5,9,4,5};

// affichertab(5,tableau);

 /*
 * Excercice 2 ---------------------
 *Ecrire une fonction void remplirtab
 *Entr�e : Argument du tableau
 *Sortie : remplir tableau d'entiers
 */

 //remplirtab(5,tableau);

  /*
 * Excercice 3 ---------------------
 *Ecrire une fonction sommertab
 *Entr�e : Argument du tableau
 *Sortie : calcul de la somme des elements contenus dans le tableau
 */

 //sommertab(5,tableau);

   /*
 * Excercice 4 ---------------------
 *Ecrire une fonction sommercarretab
 *Entr�e : Argument du tableau
 *Sortie : calcul de la somme des carr�s des elements contenus dans le tableau
 */

//sommercarretab(5,tableau);

   /*
 * Excercice 5 ---------------------
 *Ecrire une fonction moyennetab
 *Entr�e : Argument du tableau
 *Sortie : calcul de la moyenne des elements contenus dans le tableau
 */

//moyennetab(5,tableau);

   /*
 * Excercice 6 ---------------------
 *Ecrire une fonction int max
 *Entr�e : Argument du tableau
 *Sortie : recherche l'element maximal des elements contenus dans le tableau
 */

//max(5,tableau);

   /*
 * Excercice 7 ---------------------
 *Ecrire une fonction int min
 *Entr�e : Argument du tableau
 *Sortie : recherche l'element minimal des elements contenus dans le tableau
tableauminmax[2]={min(taille,tableau),max(taille,tableau}; */

//min(5,tableau);

   /*
 * Excercice 8 ---------------------
 *Ecrire une fonction int minmax met dans le tableau tabMinMax le minimum du
 tableau tab dans sa premi�re case, et le max dans sa seconde case.
 La fonction ne renvoie rien
 *Entr�e : Argument du tableau
 *Sortie : recherche l'element minimal et maximal des elements contenus dans le tableau
 */
/*
int tableauminmax[2];
minmax(5,tableau,tableauminmax);
affichertab(2,tableauminmax);
*/

   /*
 * Excercice 9 ---------------------
 *Occurences permet de savoir si  un nombre nb est pr�sent dans un tableau d�entiers en argument
 *Entr�e : Argument du tableau
 *Sortie : Cette fonction renvoie 1 s�il est pr�sent, et 0 sinon
 *--a--
 */

//int nb;
//printf("%d\n",testoccurrence(5,tableau,nb));

//--b-- Fonction ) qui permet de calculer combien de fois un nombre nb se retrouve dans le tableau

//int nb2=9;
//printf("%d\n",calculnboccurences(5,tableau,nb2));

//--c-- Fonction renvoie la position de la premi�re occurrence de nb dans le tableau tab

//int nb=5;
//printf("%d\n",pospremiereoccurrence(5,tableau,nb));

//--d-- Fonction qui rempli le tableau tabIndexes avec la position de toutes les occurrences du nombre nb dans le tableau

/*
int s=calculnboccurences(5,tableau,9);
int tableauindexes[s];

tableauoccurrences(5,tableau,tableauindexes,9);
affichertab(s,tableauindexes);*/

   /*
 * Excercice 10 ---------------------
 *Inversion de tableau
 *Entr�e : Argument du tableau
 *Sortie : tableau invers�
 *--a-- */
/*int tableau[5]={9,4,8,3,1};
int tabinv[5];


inversertab(5,tableau,tabinv);
affichertab(5,tabinv);
*/
//--b--
/*
int taille=5;

inversertab2(5,tableau);
printf("Tableau inverse :\n");

for (int i=0; i<taille; i++)
{
    printf("%d ", tableau[i]);
}
*/



    return 0;
}
